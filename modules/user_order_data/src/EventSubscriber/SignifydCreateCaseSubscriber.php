<?php

namespace Drupal\user_order_data\EventSubscriber;

use Drupal\commerce_exchanger\ExchangerCalculatorInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_signifyd\Event\SignifydCreateCaseEvent;
use Drupal\commerce_signifyd\Event\SignifydEvents;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Adds value for AggregateOrderDollars in payload data for createCase.
 */
class SignifydCreateCaseSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The commerce exchange calculate service.
   *
   * @var \Drupal\commerce_exchanger\ExchangerCalculatorInterface
   */
  protected $commerceExchangerCalculate;

  /**
   * Creates a new SignifydCreateCaseSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_exchanger\ExchangerCalculatorInterface $commerce_exchanger_calculate
   *   The commerce exchange calculate service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ExchangerCalculatorInterface $commerce_exchanger_calculate) {
    $this->entityTypeManager = $entity_type_manager;
    $this->commerceExchangerCalculate = $commerce_exchanger_calculate;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      SignifydEvents::SIGNIFYD_CREATE_CASE => ['beforeCreateCase'],
    ];
  }

  /**
   * Sets value for aggregate number in USD.
   *
   * @param \Drupal\commerce_signifyd\Event\SignifydCreateCaseEvent $event
   *   The event.
   */
  public function beforeCreateCase(SignifydCreateCaseEvent $event) {
    $case = $event->getCase();
    if (!empty($case['userAccount']['accountNumber'])) {
      $uid = $case['userAccount']['accountNumber'];
      $user = $this->entityTypeManager->getStorage('user')->load($uid);
      $user_orders = $this->entityTypeManager->getStorage('commerce_order')->loadByProperties(
        [
          'uid' => $user->id(),
          'cart' => 0,
        ]);
      $aggregate_order_dollars = new Price(0, 'USD');
      /** @var \Drupal\commerce_order\Entity\OrderInterface $user_order */
      foreach ($user_orders as $user_order) {
        if ($user_order->getTotalPrice()) {
          if ($user_order->getTotalPrice()->getCurrencyCode() === 'USD') {
            $aggregate_order_dollars = $aggregate_order_dollars->add($user_order->getTotalPrice());
          }
          else {
            $converted_total_price = $this->commerceExchangerCalculate->priceConversion($user_order->getTotalPrice(), 'USD');
            $aggregate_order_dollars = $aggregate_order_dollars->add($converted_total_price);
          }
        }
      }

      $last_order = end($user_orders);

      $case['userAccount']['lastOrderId'] = ($last_order instanceof OrderInterface) ? $last_order->id() : NULL;
      $case['userAccount']['lastUpdateDate'] = ($last_order instanceof OrderInterface) ? date(DATE_ATOM, $last_order->getChangedTime()) : NULL;
      $case['userAccount']['aggregateOrderDollars'] = $aggregate_order_dollars->getNumber();
      $case['userAccount']['aggregateOrderCount'] = count($user_orders);
      $event->setCase($case);
    }

  }

}
