<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Entity;

use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;

/**
 * Tests the Case entity.
 *
 * @coversDefaultClass \Drupal\commerce_signifyd\Entity\SignifydCase
 *
 * @group commerce_signifyd
 */
class CaseTest extends OrderKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_signifyd',
  ];

  /**
   * The case storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $caseStorage;

  /**
   * The case.
   *
   * @var \Drupal\commerce_signifyd\Entity\SignifydCaseInterface
   */
  protected $case;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('signifyd_case');

    $this->caseStorage = $this->container->get('entity_type.manager')->getStorage('signifyd_case');
    $this->case = $this->caseStorage->create([
      'case_id' => 1,
      'order_id' => 1,
    ]);
  }

  /**
   * Tests the case entity.
   *
   * @covers ::getOrderId
   * @covers ::getOrder
   * @covers ::getStatus
   * @covers ::setStatus
   * @covers ::getScore
   * @covers ::setScore
   * @covers ::getGuarantee
   * @covers ::setGuarantee
   * @covers ::getDecision
   * @covers ::setDecision
   */
  public function testCase() {
    $this->assertEquals(1, $this->case->getOrderId());

    $this->assertEquals(NULL, $this->case->getStatus());
    $this->case->setStatus('PENDING');
    $this->assertEquals('PENDING', $this->case->getStatus());

    $this->assertEquals(0, $this->case->getScore());
    $this->case->setScore(100);
    $this->assertEquals(100, $this->case->getScore());

    $this->assertEquals(NULL, $this->case->getGuarantee());
    $this->case->setGuarantee('APPROVED');
    $this->assertEquals('APPROVED', $this->case->getGuarantee());

    $this->assertEquals(NULL, $this->case->getDecision());
    $this->case->setDecision('ACCEPT');
    $this->assertEquals('ACCEPT', $this->case->getDecision());

    $this->assertEquals(NULL, $this->case->getInvestigationId());
    $this->case->setInvestigationId(1);
    $this->assertEquals(1, $this->case->getInvestigationId());
  }

}
