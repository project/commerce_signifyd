<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Signifyd;

/**
 * Tests the cases creation calls.
 *
 * @coversDefaultClass \Drupal\commerce_signifyd\Signifyd\Events
 *
 * @group commerce_signifyd
 */
class EventsTest extends AbstractSignifydKernelBase {

  /**
   * @covers ::orderFulfilled
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderFulfillSubscriber::onOrderFulfill
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderOperation
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderEvent
   */
  public function testOrderFulfillNoQueue() {
    $this->order->getState()->applyTransitionById('validate');
    $this->order->save();
    $this->order->getState()->applyTransitionById('fulfill');
    $this->order->save();

    $this->assertEquals('completed', $this->order->getState()->getId());
  }

}
