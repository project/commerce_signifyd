<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Signifyd;

/**
 * Tests the cases creation calls.
 *
 * @coversDefaultClass \Drupal\commerce_signifyd\Signifyd\Guarantees
 *
 * @group commerce_signifyd
 */
class GuaranteesTest extends AbstractSignifydKernelBase {

  /**
   * @covers ::orderFulfilled
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderCancelSubscriber::onOrderCancel
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderOperation
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderEvent
   */
  public function testOrderCancelNoQueue() {
    $this->order->getState()->applyTransitionById('cancel');
    $this->order->save();

    $this->assertEquals('canceled', $this->order->getState()->getId());
  }

}
