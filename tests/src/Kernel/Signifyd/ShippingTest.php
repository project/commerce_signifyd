<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Signifyd;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\Shipment;
use Drupal\commerce_shipping\Entity\ShippingMethod;
use Drupal\commerce_shipping\ShipmentItem;
use Drupal\physical\Weight;

/**
 * Tests the cases creation call with commerce shipping.
 *
 * @coversDefaultClass \Drupal\commerce_signifyd\Signifyd\Cases
 *
 * @group commerce_signifyd
 *
 * @requires module physical
 * @requires module commerce_shipping
 */
class ShippingTest extends AbstractSignifydKernelBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'physical',
    'path',
    'commerce_shipping',
    'commerce_shipping_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('commerce_shipping_method');
    $this->installEntitySchema('commerce_shipment');
    $this->installConfig([
      'profile',
      'commerce_product',
      'commerce_order',
      'commerce_shipping',
    ]);

    // Install the variation trait.
    $trait_manager = $this->container->get('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_shippable');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = OrderType::load('default');
    $order_type->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default');
    $order_type->setWorkflowId('order_fulfillment_signifyd_validation');
    $order_type->save();
    // Create the order field.
    $field_definition = commerce_shipping_build_shipment_field_definition($order_type->id());
    $this->container->get('commerce.configurable_field_manager')->createField($field_definition);

    $this->reloadEntity($order_type);

    $order = Order::create([
      'type' => 'default',
      'mail' => $this->order->getCustomer(),
      'uid' => $this->order->getCustomerId(),
      'ip_address' => '127.0.0.1',
      'order_number' => '2017/01',
      'billing_profile' => $this->order->getBillingProfile(),
      'store_id' => $this->store->id(),
      'order_items' => $this->order->getItems(),
      'state' => 'draft',
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);

    $shipping_method = ShippingMethod::create([
      'stores' => $this->store->id(),
      'name' => 'Example',
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'rate_label' => 'Flat rate',
          'rate_amount' => new Price('1', 'USD'),
        ],
      ],
      'status' => TRUE,
      'weight' => 1,
    ]);
    $shipping_method->save();

    $shipment = Shipment::create([
      'type' => 'default',
      'order_id' => $order->id(),
      'title' => 'Shipment',
      'shipping_method' => $shipping_method,
      'shipping_profile' => $order->getBillingProfile(),
      'items' => [
        new ShipmentItem([
          'order_item_id' => 10,
          'title' => 'T-shirt (red, large)',
          'quantity' => 2,
          'weight' => new Weight('40', 'kg'),
          'declared_value' => new Price('30', 'USD'),
        ]),
      ],
      'amount' => new Price('5', 'USD'),
      'state' => 'draft',
    ]);
    $shipment->save();
    $this->shipment = $this->reloadEntity($shipment);

    $this->order->set('shipments', [$shipment]);
    $this->order->save();
    $this->reloadEntity($this->order);
  }

  /**
   * @covers ::createCase
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderPlaceSubscriber::onOrderPlace
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderCancelSubscriber::onOrderCancel
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderFulfillSubscriber::onOrderFulfill
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderOperation
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderEvent
   */
  public function testOrderEventsNoQueue() {
    $this->assertEquals('draft', $this->order->getState()->getId());

    $this->order->getState()->applyTransitionById('place');
    $this->order->save();

    $this->assertEquals('validation', $this->order->getState()->getId());

    $this->order->getState()->applyTransitionById('validate');
    $this->order->save();

    $this->assertEquals('fulfillment', $this->order->getState()->getId());

    $this->order->getState()->applyTransitionById('fulfill');
    $this->order->save();

    $this->assertEquals('completed', $this->order->getState()->getId());

    $this->order->set('state', 'fulfillment');
    $this->order->save();

    $this->reloadEntity($this->order);

    $this->order->getState()->applyTransitionById('cancel');
    $this->order->save();

    $this->assertEquals('canceled', $this->order->getState()->getId());

  }

}
