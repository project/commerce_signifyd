<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Signifyd;

/**
 * Tests the cases creation calls.
 *
 * @coversDefaultClass \Drupal\commerce_signifyd\Signifyd\Cases
 *
 * @group commerce_signifyd
 */
class CasesTest extends AbstractSignifydKernelBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->order->set('state', 'draft');
    $this->order->save();
    $this->reloadEntity($this->order);
  }

  /**
   * @covers ::createCase
   * @covers \Drupal\commerce_signifyd\EventSubscriber\OrderPlaceSubscriber::onOrderPlace
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderOperation
   * @covers \Drupal\commerce_signifyd\SignifydTask::processOrderEvent
   */
  public function testOrderPlacementNoQueue() {
    $this->order->getState()->applyTransitionById('place');
    $this->order->save();

    $this->assertEquals('validation', $this->order->getState()->getId());
  }

}
