<?php

namespace Drupal\Tests\commerce_signifyd\Kernel\Signifyd;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\commerce_signifyd\Entity\SignifydTeam;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\profile\Entity\Profile;
use Drupal\Tests\commerce_order\Kernel\OrderKernelTestBase;
use Drupal\Tests\commerce_signifyd\SignifydTestClient;

/**
 * Base class for kernel tests.
 */
abstract class AbstractSignifydKernelBase extends OrderKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $service_definition = $container->getDefinition('commerce_signifyd.client');
    $service_definition->setClass(SignifydTestClient::class);
    if ($container->hasDefinition('commerce_order.entity_accessible_availability_checker')) {
      $container->removeDefinition('commerce_order.entity_accessible_availability_checker');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_signifyd',
    'commerce_log',
    'commerce_payment',
    'commerce_payment_example',
  ];

  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * A sample variation.
   *
   * @var \Drupal\commerce_product\Entity\ProductVariationInterface
   */
  protected $variation;

  /**
   * The order receipt.
   *
   * @var \Drupal\commerce_signifyd\Signifyd\CasesInterface
   */
  protected $signifydCases;

  /**
   * The Signifyd team.
   *
   * @var \Drupal\commerce_signifyd\Entity\SignifydTeamInterface
   */
  protected $signifydTeam;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('signifyd_case');
    $this->installEntitySchema('signifyd_team');
    $this->installConfig('commerce_signifyd');
    $this->installEntitySchema('commerce_log');
    $this->installEntitySchema('commerce_payment');
    $this->installConfig('commerce_payment');

    $signifyd_team = SignifydTeam::create([
      'id' => 'test',
      'label' => 'Test label',
      'status' => TRUE,
      'api_key' => 'irieir848t48t845jerjf',
      'team_id' => 1234,
    ]);
    $signifyd_team->save();

    $this->signifydTeam = $signifyd_team;

    $this->config('commerce_signifyd.settings')->setData([
      "team" => "test",
      // Important for payload / request testing.
      "queue" => FALSE,
      "logging" => TRUE,
      "decision_type" => "guarantee",
      "score" => 700,
      "order_types" => [
        "default" => [
          "enable" => 1,
          "workflow" => 1,
          "approved" => "validate",
          "declined" => "flagged_fraud",
        ],
      ],
      "conditions" => [
        "payment_gateways" => ["example_credit_card" => "0"],
        "customer_roles" => [
          "anonymous" => "0",
          "authenticated" => "0",
          "administrator" => "0",
        ],
      ],
    ])->save();

    $user = $this->createUser([], NULL, FALSE, [
      'mail' => 'customer@example.com',
      'preferred_langcode' => 'en',
    ]);

    // Turn off title generation to allow explicit values to be used.
    $variation_type = ProductVariationType::load('default');
    $variation_type->setGenerateTitle(FALSE);
    $variation_type->save();

    $product = Product::create([
      'type' => 'default',
      'title' => 'Default testing product',
    ]);
    $product->save();

    $variation1 = ProductVariation::create([
      'type' => 'default',
      'sku' => 'TEST_' . strtolower($this->randomMachineName()),
      'title' => $this->randomString(),
      'status' => 1,
      'price' => new Price('12.00', 'USD'),
    ]);
    $variation1->save();
    $this->variation = $variation1;
    $product->addVariation($variation1)->save();

    $profile = Profile::create([
      'type' => 'customer',
      'address' => [
        'country_code' => 'US',
        'postal_code' => '53177',
        'locality' => 'Milwaukee',
        'address_line1' => 'Pabst Blue Ribbon Dr',
        'administrative_area' => 'WI',
        'given_name' => 'Frederick',
        'family_name' => 'Pabst',
      ],
    ]);
    $profile->save();
    $profile = $this->reloadEntity($profile);

    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = $this->container->get('entity_type.manager')
      ->getStorage('commerce_order_type')
      ->load('default');
    $order_type->setWorkflowId('order_fulfillment_signifyd_validation');
    $order_type->save();

    /** @var \Drupal\commerce_order\OrderItemStorageInterface $order_item_storage */
    $order_item_storage = $this->container->get('entity_type.manager')
      ->getStorage('commerce_order_item');
    $order_item1 = $order_item_storage->createFromPurchasableEntity($variation1);
    $order_item1->save();
    $order = Order::create([
      'type' => 'default',
      'mail' => $user->getEmail(),
      'uid' => $user->id(),
      'ip_address' => '127.0.0.1',
      'order_number' => '2017/01',
      'billing_profile' => $profile,
      'store_id' => $this->store->id(),
      'order_items' => [$order_item1],
      'state' => 'validation',
    ]);
    $order->save();
    $this->order = $this->reloadEntity($order);
  }

}
