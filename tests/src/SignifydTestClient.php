<?php

namespace Drupal\Tests\commerce_signifyd;

use Drupal\commerce_signifyd\Entity\SignifydTeamInterface;
use Drupal\commerce_signifyd\SignifydClient;

/**
 * {@inheritdoc}
 */
class SignifydTestClient extends SignifydClient {

  /**
   * We use only this to verify that payload is built correctly.
   *
   * {@inheritdoc}
   */
  public function apiCall(string $endpoint, SignifydTeamInterface $signifyd_team, array $payload = [], string $method = 'GET') {
    return ['investigationID' => 1];
  }

}
