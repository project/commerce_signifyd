<?php

namespace Drupal\commerce_signifyd\ParamConverter;

use Drupal\Core\ParamConverter\EntityConverter;
use Symfony\Component\Routing\Route;

/**
 * Converts parameter for upcasting team id to Signifyd team entity id.
 */
class SignifydParamConverter extends EntityConverter {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    $entity_type_id = $this->getEntityTypeFromDefaults($definition, $name, $defaults);
    if ($entity_type_id === 'signifyd_team') {
      if (is_numeric($value)) {
        if ($storage = $this->entityTypeManager->getStorage($entity_type_id)) {
          if (!$teams = $storage->loadByProperties(['team_id' => $value])) {
            return NULL;
          }

          return reset($teams);
        }
      }
    }
    return $this->entityRepository->getCanonical($entity_type_id, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return $name === "signifyd_team";
  }

}
