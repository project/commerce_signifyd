<?php

namespace Drupal\commerce_signifyd\Event;

use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired during webhook event call.
 *
 * @package Drupal\commerce_signifyd\Event
 */
class SignifydWebhookEvent extends Event {

  /**
   * The Signifyd case entity.
   *
   * @var \Drupal\commerce_signifyd\Entity\SignifydCaseInterface
   */
  public $signifydCase;

  /**
   * The webhook payload.
   *
   * @var array
   */
  public $payload;

  /**
   * The entity action.
   *
   * @var string
   */
  public $action;

  /**
   * The topic.
   *
   * @var string
   */
  public $topic;

  /**
   * Constructs a new SignifydSendTransactionsEvent object.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $signifyd_case
   *   The Signifyd Case.
   * @param array $payload
   *   The webhook payload.
   * @param string $action
   *   The CRUD action of Signifyd case.
   * @param string $topic
   *   The webhook topic.
   */
  public function __construct(SignifydCaseInterface $signifyd_case, array $payload, string $action, string $topic) {
    $this->signifydCase = $signifyd_case;
    $this->payload = $payload;
    $this->action = $action;
    $this->topic = $topic;
  }

  /**
   * Gets the webhook payload.
   *
   * @return array
   *   The payload.
   */
  public function getPayload() {
    return $this->payload;
  }

  /**
   * Get the Signifyd case.
   *
   * @return \Drupal\commerce_signifyd\Entity\SignifydCaseInterface
   *   The Signifyd Case.
   */
  public function getSignifydCase() {
    return $this->signifydCase;
  }

  /**
   * Get the action.
   *
   * @return string
   *   The state.
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * Get the topic.
   *
   * @return string
   *   The topic.
   */
  public function getTopic() {
    return $this->topic;
  }

}
