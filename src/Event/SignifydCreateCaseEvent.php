<?php

namespace Drupal\commerce_signifyd\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_signifyd\Entity\SignifydTeamInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired before sending case to Signifyd.
 *
 * @package Drupal\commerce_signifyd\Event
 */
class SignifydCreateCaseEvent extends Event {

  /**
   * The case object.
   *
   * @var array
   */
  public $case;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  public $order;

  /**
   * The Signifyd team.
   *
   * @var \Drupal\commerce_signifyd\Entity\SignifydTeamInterface
   */
  public $team;

  /**
   * Constructs a new SignifydCreateCaseEvent object.
   *
   * @param array $case
   *   The Case payload.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $signifyd_team
   *   The Signifyd team.
   */
  public function __construct(array $case, OrderInterface $order, SignifydTeamInterface $signifyd_team) {
    $this->case = $case;
    $this->order = $order;
    $this->team = $signifyd_team;
  }

  /**
   * Set the case.
   *
   * @param array $case
   *   The Case payload to be set.
   *
   * @return $this
   */
  public function setCase(array $case) {
    $this->case = $case;
    return $this;
  }

  /**
   * Get the case.
   *
   * @return array
   *   The case array.
   */
  public function getCase() {
    return $this->case;
  }

  /**
   * Set the team.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $team
   *   The Signifyd team.
   *
   * @return $this
   */
  public function setTeam(SignifydTeamInterface $team) {
    $this->team = $team;
    return $this;
  }

  /**
   * Get the current team.
   *
   * @return \Drupal\commerce_signifyd\Entity\SignifydTeamInterface
   *   The Signifyd team.
   */
  public function getTeam() {
    return $this->team;
  }

  /**
   * Get the case.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
