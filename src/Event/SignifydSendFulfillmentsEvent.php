<?php

namespace Drupal\commerce_signifyd\Event;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired before sending fulfillment's.
 *
 * @package Drupal\commerce_signifyd\Event
 */
class SignifydSendFulfillmentsEvent extends Event {

  /**
   * Fulfillment to be sent.
   *
   * @var array
   */
  public $fulfillments;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  public $order;

  /**
   * Constructs a new SignifydFulfillmentsEvent object.
   *
   * @param array $fulfillments
   *   The fulfillment.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The commerce order.
   */
  public function __construct(array $fulfillments, OrderInterface $order) {
    $this->fulfillments = $fulfillments;
    $this->order = $order;
  }

  /**
   * Gets the fulfillment.
   *
   * @return array
   *   The fulfillment.
   */
  public function getFulfillments(): array {
    return $this->fulfillments;
  }

  /**
   * Set the fulfillment.
   *
   * @param array $fulfillment
   *   The fulfillment.
   *
   * @return $this
   */
  public function setFulfillments(array $fulfillment) {
    $this->fulfillments = $fulfillment;
    return $this;
  }

  /**
   * Get the case.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The order.
   */
  public function getOrder() {
    return $this->order;
  }

}
