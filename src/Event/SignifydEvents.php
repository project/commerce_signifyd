<?php

namespace Drupal\commerce_signifyd\Event;

/**
 * Provides final class.
 */
final class SignifydEvents {

  /**
   * Name of the event fired before case is sent to Signifyd.
   *
   * Allow altering payload of case before is sent to Signifyd.
   *
   * @Event
   *
   * @see \Drupal\commerce_signifyd\Event\SignifydCreateCaseEvent
   */
  const SIGNIFYD_CREATE_CASE = 'signifyd_create_case';

  /**
   * Name of the event fired before fulfillment are sent to Signifyd.
   *
   * Allow altering payload of fulfillment's before is sent to Signifyd.
   *
   * @Event
   *
   * @see \Drupal\commerce_signifyd\Event\SignifydSendFulfillmentsEvent
   */
  const SIGNIFYD_SEND_FULFILLMENTS = 'signifyd_send_fulfillments';

  /**
   * Name of the event fired after webhook update.
   *
   * Allow reacting after Signifyd sent webhook updates.
   *
   * @Event
   *
   * @see \Drupal\commerce_signifyd\Event\SignifydWebhookEvent
   */
  const SIGNIFYD_WEBHOOK = 'signifyd_webhook';

}
