<?php

namespace Drupal\commerce_signifyd;

use Drupal\Core\Entity\EntityListBuilder;

/**
 * Defines the list builder for Signifyd cases.
 */
class SignifydCaseListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['case_id'] = $this->t('Case ID');
    $header['order_id'] = $this->t('Order ID');
    $header['score'] = $this->t('Score');
    $header['guarantee'] = $this->t('Guarantee');
    return $header + parent::buildHeader();
  }

}
