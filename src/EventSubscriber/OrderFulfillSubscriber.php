<?php

namespace Drupal\commerce_signifyd\EventSubscriber;

use Drupal\commerce_signifyd\SignifydTaskInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Notify Signifyd on successful fulfillment of the order.
 */
class OrderFulfillSubscriber implements EventSubscriberInterface {

  /**
   * The helper service.
   *
   * @var \Drupal\commerce_signifyd\SignifydTaskInterface
   */
  protected $signifydTask;

  /**
   * Constructs a new OrderFulfillSubscriber object.
   *
   * @param \Drupal\commerce_signifyd\SignifydTaskInterface $signifyd_task
   *   The helper service.
   */
  public function __construct(SignifydTaskInterface $signifyd_task) {
    $this->signifydTask = $signifyd_task;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Execute events as late possible, so that Signifyd is last. After
    // all contrib and potentially custom logic.
    return [
      'commerce_order.fulfill.post_transition' => ['onOrderFulfill', -1000],
    ];
  }

  /**
   * Triggers sending fulfillment.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   */
  public function onOrderFulfill(WorkflowTransitionEvent $event) {
    $this->signifydTask->processOrderEvent($event->getEntity(), SignifydTaskInterface::OPERATION_SEND_FULFILLMENT);
  }

}
