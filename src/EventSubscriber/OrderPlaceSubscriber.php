<?php

namespace Drupal\commerce_signifyd\EventSubscriber;

use Drupal\commerce_signifyd\SignifydTaskInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Create Signifyd case on order placement.
 */
class OrderPlaceSubscriber implements EventSubscriberInterface {

  /**
   * The helper service.
   *
   * @var \Drupal\commerce_signifyd\SignifydTaskInterface
   */
  protected $signifydTask;

  /**
   * Constructs a new OrderPlaceSubscriber object.
   *
   * @param \Drupal\commerce_signifyd\SignifydTaskInterface $signifyd_task
   *   The helper service.
   */
  public function __construct(SignifydTaskInterface $signifyd_task) {
    $this->signifydTask = $signifyd_task;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    // Execute events as late possible, so that Signify is last. After
    // all contrib and potentially custom logic.
    return [
      'commerce_order.place.post_transition' => ['onOrderPlace', -1000],
    ];
  }

  /**
   * Triggers sending case if integration is enabled for that order type.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    $this->signifydTask->processOrderEvent($event->getEntity(), SignifydTaskInterface::OPERATION_CREATE_CASE);
  }

}
