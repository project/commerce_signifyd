<?php

namespace Drupal\commerce_signifyd\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Signify team default form.
 *
 * @property \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $entity
 */
class SignifydTeamForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Team name'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Administration name for the team.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_signifyd\Entity\SignifydTeam::load',
      ],
      '#disabled' => !$this->entity->isNew(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['team_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Signifyd team id'),
      '#min' => 1,
      '#default_value' => $this->entity->getTeamId(),
      '#description' => $this->t('It represents team created on Signifyd dashboard. Team ID is numeric, and it is last parameter in URL when you are on team settings page in Signifyd - https://app.signifyd.com/settings/teams/{your_numeric_team_id}.'),
      '#required' => TRUE,
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->getApiKey(),
      '#description' => $this->t('API key for the Signifyd team.'),
      '#required' => TRUE,
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
      ? $this->t('Created new Signifyd team %label.', $message_args)
      : $this->t('Updated Signifyd team %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
