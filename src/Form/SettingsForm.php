<?php

namespace Drupal\commerce_signifyd\Form;

use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_signifyd\Entity\SignifydTeam;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Commerce Signifyd settings for this site.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The workflow manager.
   *
   * @var \Drupal\state_machine\WorkflowManagerInterface
   */
  protected $workflowManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Construct SettingsForm class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, WorkflowManagerInterface $workflow_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->workflowManager = $workflow_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.workflow'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_signifyd_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_signifyd.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_signifyd.settings');
    $teams = $this->entityTypeManager->getStorage('signifyd_team')->loadByProperties(['status' => TRUE]);

    $team_options = array_map(function (SignifydTeam $team) {
      return $team->label();
    }, $teams);

    if (empty($team_options)) {
      $form['team'] = [
        '#type' => '#markup',
        '#markup' => $this->t('You need to create and enable at-least one team. @url', ['@url' => Link::createFromRoute('Create or/and enable a team.', 'entity.signifyd_team.collection')->toString()]),
      ];
      return $form;
    }

    $form['team'] = [
      '#type' => 'select',
      '#options' => $team_options,
      '#required' => TRUE,
      '#title' => $this->t('Default team'),
      '#default_value' => $config->get('team'),
    ];

    $form['queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Queue Signifyd requests'),
      '#default_value' => $config->get('queue') ?? FALSE,
    ];

    $form['logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log API calls'),
      '#default_value' => $config->get('logging') ?? FALSE,
    ];

    $form['decision_type'] = [
      '#type' => 'radios',
      '#options' => [
        'decision' => $this->t('DECISION - recommended integration, require setup of @url webhook.', ['@url' => Link::fromTextAndUrl('Decision Made', Url::fromUri('https://developer.signifyd.com/api-v2/#/decision-made'))->toString()]),
        'guarantee' => $this->t('GUARANTEE - deprecated in v3, require setup of @url	webhook', ['@url' => Link::fromTextAndUrl('Case Creation', Url::fromUri('https://developer.signifyd.com/api-v2/#/case-webhook'))->toString()]),
        'score' => $this->t('SCORE - based on score, only to be used in advanced scenarios. Does not necessary provide decision with financial guarantee'),
      ],
      '#description' => $this->t('This is used only if you enable for any order type automatic workflow integration to transition order based on your selection here'),
      '#title' => $this->t('Decision type'),
      '#default_value' => $config->get('decision_type') ?? 'decision',
    ];

    $form['score'] = [
      '#type' => 'number',
      '#min' => 100,
      '#max' => 1000,
      '#title' => $this->t('Score'),
      '#default_value' => $config->get('score') ?? 700,
      '#states' => [
        'visible' => [
          ':input[name="decision_type"]' => ['value' => 'score'],
        ],
      ],
    ];

    $form['order_types'] = [
      '#type' => 'details',
      '#title' => $this->t('Order integration'),
      '#weight' => 5,
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $order_integration = $config->get('order_types');

    $order_types = $this->entityTypeManager->getStorage('commerce_order_type')->loadMultiple();

    foreach ($order_types as $order_type) {
      $machine_name = $order_type->id();
      $form['order_types'][$machine_name] = [
        '#type' => 'details',
        '#title' => $this->t('Order type: @order_type', ['@order_type' => $order_type->label()]),
        '#weight' => 5,
        '#open' => FALSE,
      ];
      $form['order_types'][$machine_name]['enable'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable Signifyd integration'),
        '#description' => $this->t('Enable Signifyd integration. This enables creating cases and receiving Signifyd decisions.'),
        '#default_value' => $order_integration[$machine_name]['enable'] ?? FALSE,
      ];

      $form['order_types'][$machine_name]['workflow'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable automatic workflow integration'),
        '#description' => $this->t('Enable this option if you want to automate order transitions based on Signifyd decisions.'),
        '#default_value' => $order_integration[$machine_name]['workflow'] ?? FALSE,
        '#states' => [
          'visible' => [
            ':input[name="order_types[' . $machine_name . '][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $workflow_definition = $this->workflowManager->getDefinition($order_type->getWorkflowId());
      $transitions = $workflow_definition['transitions'] ?? [];

      // Don't allow place transition to be used.
      unset($transitions['place']);

      $transitions = array_map(function (array $transition) {
        return $transition['label'];
      }, $transitions);

      $form['order_types'][$machine_name]['declined'] = [
        '#type' => 'select',
        '#title' => $this->t('Declined'),
        '#description' => $this->t('Transition to trigger if order is declined by Signifyd'),
        '#options' => $transitions,
        '#default_value' => $order_integration[$machine_name]['declined'] ?? NULL,
        '#states' => [
          'visible' => [
            ':input[name="order_types[' . $machine_name . '][workflow]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['order_types'][$machine_name]['approved'] = [
        '#type' => 'select',
        '#title' => $this->t('Approved'),
        '#description' => $this->t('Transition to trigger if order is approved by Signifyd.'),
        '#options' => $transitions,
        '#default_value' => $order_integration[$machine_name]['approved'] ?? NULL,
        '#states' => [
          'visible' => [
            ':input[name="order_types[' . $machine_name . '][workflow]"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    $conditions = $config->get('conditions');

    $form['conditions'] = [
      '#type' => 'details',
      '#title' => $this->t('Conditions (exclusions)'),
      '#description' => $this->t('Select conditions for orders which should not be processed by Signifyd'),
      '#weight' => 5,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    /** @var PaymentGatewayInterface[] $payment_gateways */
    $payment_gateways = $this->entityTypeManager->getStorage('commerce_payment_gateway')->loadMultiple();
    $payment_method_options = [];
    $payment_gateways_options = [];
    foreach ($payment_gateways as $payment_gateway) {
      $payment_gateways_options[$payment_gateway->id()] = $payment_gateway->label();
      $payment_method_types = $payment_gateway->getPlugin()->getPaymentMethodTypes();

      foreach ($payment_method_types as $id => $payment_method_type) {
        $payment_method_options[$id] = $payment_method_type->getLabel();
      }}

    $form['conditions']['payment_gateways'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment gateways'),
      '#options' => $payment_gateways_options,
      '#default_value' => $conditions['payment_gateways'] ?? [],
    ];

    $form['conditions']['payment_methods'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Payment methods'),
      '#options' => $payment_method_options,
      '#default_value' => $conditions['payment_methods'] ?? [],
    ];

    $user_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    $user_roles = array_map(function (RoleInterface $user_roles) {
      return $user_roles->label();
    }, $user_roles);

    $form['conditions']['customer_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Customer role'),
      '#options' => $user_roles,
      '#default_value' => $conditions['customer_roles'] ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_signifyd.settings')
      ->set('team', $form_state->getValue('team'))
      ->set('queue', $form_state->getValue('queue'))
      ->set('logging', $form_state->getValue('logging'))
      ->set('decision_type', $form_state->getValue('decision_type'))
      ->set('score', $form_state->getValue('score'))
      ->set('order_types', $form_state->getValue('order_types'))
      ->set('conditions', $form_state->getValue('conditions'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
