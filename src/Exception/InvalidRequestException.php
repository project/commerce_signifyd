<?php

namespace Drupal\commerce_signifyd\Exception;

/**
 * Handling exceptions.
 */
class InvalidRequestException extends \RuntimeException {}
