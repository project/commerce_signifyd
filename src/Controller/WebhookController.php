<?php

namespace Drupal\commerce_signifyd\Controller;

use Drupal\commerce_signifyd\Entity\SignifydTeam;
use Drupal\commerce_signifyd\Signifyd\WebhookInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for Commerce Signifyd routes.
 */
class WebhookController extends ControllerBase {

  /**
   * The webhook object.
   *
   * @var \Drupal\commerce_signifyd\Signifyd\WebhookInterface
   *   The Signifyd Webhook.
   */
  protected $webhook;

  /**
   * Constructs a new WebhookController object.
   *
   * @param \Drupal\commerce_signifyd\Signifyd\WebhookInterface $webhook
   *   The Signifyd Webhook.
   */
  public function __construct(WebhookInterface $webhook) {
    $this->webhook = $webhook;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_signifyd.webhook')
    );
  }

  /**
   * Builds the response.
   */
  public function build(Request $request, SignifydTeam $signifyd_team) {
    return $this->webhook->events($request, $signifyd_team);
  }

}
