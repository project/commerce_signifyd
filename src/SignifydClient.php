<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;
use Drupal\commerce_signifyd\Entity\SignifydTeamInterface;
use Drupal\commerce_signifyd\Exception\InvalidRequestException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelTrait;
use GuzzleHttp\Client;

/**
 * {@inheritdoc}
 */
class SignifydClient implements SignifydClientInterface {

  use LoggerChannelTrait;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The immutable configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Client constructor.
   *
   * @param \GuzzleHttp\Client $client
   *   The client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleExtensionList $module_extension_list
   *   The module extension list.
   */
  public function __construct(Client $client, ConfigFactoryInterface $config_factory, ModuleExtensionList $module_extension_list) {
    $this->client = $client;
    $this->configFactory = $config_factory;
    $this->settings = $config_factory->get('commerce_signifyd.settings');
    $this->moduleExtensionList = $module_extension_list;
  }

  /**
   * {@inheritdoc}
   */
  public function createCase(array $payload, SignifydTeamInterface $signifyd_team) {
    return $this->apiCall('cases', $signifyd_team, $payload, 'POST');
  }

  /**
   * {@inheritdoc}
   */
  public function getCase(SignifydCaseInterface $case) {
    return $this->apiCall('cases/' . $case->id(), $case->getTeam());
  }

  /**
   * {@inheritdoc}
   */
  public function sendFulfillment(SignifydCaseInterface $case, array $payload) {
    return $this->apiCall('fulfillments/' . $case->getOrderId(), $case->getTeam(), $payload, 'POST');
  }

  /**
   * {@inheritdoc}
   */
  public function sendTransaction(array $payload) {
    return $this->apiCall('transactions', NULL, $payload, 'POST');
  }

  /**
   * {@inheritdoc}
   */
  public function sendGuarantee(SignifydCaseInterface $case) {
    return $this->apiCall('async/guarantees', $case->getTeam(), ['case_id' => $case->id()], 'POST');
  }

  /**
   * {@inheritdoc}
   */
  public function cancelGuarantee(SignifydCaseInterface $case) {
    return $this->apiCall(sprintf('cases/%s/guarantee', $case->id()), $case->getTeam(), ['guaranteeDisposition' => 'CANCELED'], 'PUT');
  }

  /**
   * Helper to create ApiCalls towards Signifyd.
   *
   * @param string $endpoint
   *   The endpoint to be used.
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $signifyd_team
   *   The Signifyd team.
   * @param array $payload
   *   The payload to be sent.
   * @param string $method
   *   The method to be used.
   *
   * @return array
   *   Return formatted response.
   */
  public function apiCall(string $endpoint, SignifydTeamInterface $signifyd_team, array $payload = [], string $method = 'GET') {
    $apiToken = base64_encode($signifyd_team->getApiKey());

    $values = [
      'headers' => [
        'Content-Type' => 'application/json',
        'Authorization' => 'Basic ' . $apiToken,
      ],
    ];

    if (!empty($payload)) {
      $commerce_core = $this->moduleExtensionList->getExtensionInfo('commerce');
      $payload['platformAndClient'] = [
        'storePlatform' => 'Drupal_Commerce',
        'storePlatformVersion' => $commerce_core['version'] ?? 'dev',
        'signifydClientApp' => '',
      ];

      $values['json'] = $payload;
    }

    try {
      $request = $this->client->request($method, self::SIGNIFYD_API_URL . $endpoint, $values);
      $response = Json::decode($request->getBody());
    }
    catch (\Exception $exception) {
      $this->getLogger('commerce_signifyd')->error($exception);
      throw new InvalidRequestException($exception->getMessage(), $exception->getCode());
    }

    return $response ?? [];
  }

}
