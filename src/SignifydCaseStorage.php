<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the case storage.
 */
class SignifydCaseStorage extends CommerceContentEntityStorage implements SignifydCaseStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByOrder(OrderInterface $order) {
    $query = $this->getQuery()
      ->accessCheck()
      ->condition('order_id', $order->id());
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

}
