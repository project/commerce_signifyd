<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;
use Drupal\commerce_signifyd\Entity\SignifydTeamInterface;

/**
 * Signifyd API client interface.
 */
interface SignifydClientInterface {

  public const SIGNIFYD_API_URL = 'https://api.signifyd.com/v2/';

  public const SIGNIFYD_WEB_URL = 'https://console.signifyd.com/';

  /**
   * Create case on Signifyd.
   *
   * @param array $payload
   *   The request payload.
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $signifyd_team
   *   The Signifyd team.
   *
   * @return array
   *   The formatted response.
   */
  public function createCase(array $payload, SignifydTeamInterface $signifyd_team);

  /**
   * Fetch case from Signifyd.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The formatted response.
   */
  public function getCase(SignifydCaseInterface $case);

  /**
   * Send shipment fulfillments to Signifyd.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   * @param array $payload
   *   The request payload.
   *
   * @return array
   *   The formatted response.
   */
  public function sendFulfillment(SignifydCaseInterface $case, array $payload);

  /**
   * Send transaction to Signifyd.
   *
   * @param array $payload
   *   The request payload.
   *
   * @return array
   *   The formatted response.
   */
  public function sendTransaction(array $payload);

  /**
   * Send guarantee to Signifyd.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The formatted response.
   */
  public function sendGuarantee(SignifydCaseInterface $case);

  /**
   * Cancel guarantee on Signifyd.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The formatted response.
   */
  public function cancelGuarantee(SignifydCaseInterface $case);

}
