<?php

namespace Drupal\commerce_signifyd\Plugin\QueueWorker;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_signifyd\SignifydTaskInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'commerce_signifyd_queue' queue worker.
 *
 * @QueueWorker(
 *   id = "commerce_signifyd_queue",
 *   title = @Translation("Commerce Signifyd queue worker"),
 *   cron = {"time" = 60}
 * )
 */
class CommerceSignifydQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The helper service.
   *
   * @var \Drupal\commerce_signifyd\SignifydTaskInterface
   */
  protected $signifydTask;

  /**
   * Constructs a new CommerceSignifydQueueWorker object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\commerce_signifyd\SignifydTaskInterface $signifyd_task
   *   The helper service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, SignifydTaskInterface $signifyd_task) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->signifydTask = $signifyd_task;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_signifyd.task'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data['order_id']) || empty($data['operation'])) {
      return;
    }
    $order = $this->entityTypeManager->getStorage('commerce_order')->load($data['order_id']);
    if (!($order instanceof OrderInterface)) {
      return;
    }
    $this->signifydTask->processOrderOperation($order, $data['operation']);
  }

}
