<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_signifyd\Signifyd\CasesInterface;
use Drupal\commerce_signifyd\Signifyd\EventsInterface;
use Drupal\commerce_signifyd\Signifyd\GuaranteesInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;

/**
 * {@inheritdoc}
 */
class SignifydTask implements SignifydTaskInterface {

  /**
   * The Signifyd Case service.
   *
   * @var \Drupal\commerce_signifyd\Signifyd\Cases
   */
  protected $signifydCaseService;

  /**
   * The Signifyd Guarantees service.
   *
   * @var \Drupal\commerce_signifyd\Signifyd\GuaranteesInterface
   */
  protected $signifydGuaranteesService;

  /**
   * The Signifyd Events service.
   *
   * @var \Drupal\commerce_signifyd\Signifyd\EventsInterface
   */
  protected $signifydEventsService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The queue service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Construct new SignifydTask class.
   *
   * @param \Drupal\commerce_signifyd\Signifyd\CasesInterface $signifyd_case_service
   *   The Signifyd Case service.
   * @param \Drupal\commerce_signifyd\Signifyd\GuaranteesInterface $signifyd_guarantees_service
   *   The Signifyd Guarantees service.
   * @param \Drupal\commerce_signifyd\Signifyd\EventsInterface $signifyd_events_service
   *   The Signifyd Events service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue.
   */
  public function __construct(CasesInterface $signifyd_case_service, GuaranteesInterface $signifyd_guarantees_service, EventsInterface $signifyd_events_service, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, QueueFactory $queue_factory) {
    $this->signifydCaseService = $signifyd_case_service;
    $this->signifydGuaranteesService = $signifyd_guarantees_service;
    $this->signifydEventsService = $signifyd_events_service;
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function processOrderOperation(OrderInterface $order, string $operation) {
    // Check if integration is enabled for this order type.
    if (!$this->integrationEnabled($order)) {
      return;
    }

    switch ($operation) {
      case self::OPERATION_CREATE_CASE:
        $this->signifydCaseService->createCase($order);
        break;

      case self::OPERATION_CANCEL_GUARANTEE:
        /** @var \Drupal\commerce_signifyd\SignifydCaseStorageInterface $case_storage */
        $case_storage = $this->entityTypeManager->getStorage('signifyd_case');
        $cases = $case_storage->loadByOrder($order);
        foreach ($cases as $case) {
          $this->signifydGuaranteesService->cancel($case);
        }
        break;

      case self::OPERATION_SEND_FULFILLMENT:
        $this->signifydEventsService->orderFulfilled($order);
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processOrderEvent(OrderInterface $order, string $operation) {
    // Check if integration is enabled for this order type.
    if (!$this->integrationEnabled($order)) {
      return;
    }

    $configuration = $this->configFactory->get('commerce_signifyd.settings');

    // Check if user role is selected as excluded.
    if (array_intersect($order->getCustomer()->getRoles(), $configuration->get('conditions.customer_roles'))) {
      return;
    }

    // Check if order payment gateway is selected as excluded.
    if (in_array($order->get('payment_gateway')->target_id, $configuration->get('conditions.payment_gateways'))) {
      return;
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    if ($payment_method = $order->get('payment_method')->entity) {
      if (in_array($payment_method->getType()->getPluginId(), $configuration->get('conditions.payment_methods'))) {
        return;
      }
    }

    $config = $this->configFactory->get('commerce_signifyd.settings');
    if ($config->get('queue')) {
      $queue = $this->queueFactory->get('commerce_signifyd_queue');
      $queue->createItem(['order_id' => $order->id(), 'operation' => $operation]);
    }
    else {
      $this->processOrderOperation($order, $operation);
    }
  }

  /**
   * Check if integration is enabled.
   */
  protected function integrationEnabled(OrderInterface $order): bool {
    $configuration = $this->configFactory->get('commerce_signifyd.settings');
    $order_type = $order->bundle();

    // Check if integration is enabled for this order type.
    if (empty($configuration->get('order_types.' . $order_type . '.enable'))) {
      return FALSE;
    }

    return TRUE;
  }

}
