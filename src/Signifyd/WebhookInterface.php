<?php

namespace Drupal\commerce_signifyd\Signifyd;

use Drupal\commerce_signifyd\Entity\SignifydTeamInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides interface for Webhook class.
 */
interface WebhookInterface {

  public const SIGNIFYD_CREATE = 'create';
  public const SIGNIFYD_UPDATE = 'update';

  /**
   * List of webhooks provided by Signifyd.
   *
   * @see https://developer.signifyd.com/api/#/reference/webhooks
   */
  public const SIGNIFYD_DESCISION_MADE = 'decisions/*';
  public const SIGNIFYD_CASE_CREATION = 'cases/creation';
  public const SIGNIFYD_CASE_RESCORE = 'cases/rescore';
  public const SIGNIFYD_CASE_REVIEW = 'cases/review';
  public const SIGNIFYD_CLAIM_REVIEWED = 'claim/reviewed';
  public const SIGNIFYD_CLAIM_PAID = 'claims/paid';

  /**
   * Used with decision made webhook only when we want to store guarantee value.
   */
  public const SIGNIFYD_MAP_ACTION_TO_GUARANTEE = [
    'ACCEPT' => 'APPROVED',
    'REJECT' => 'DECLINED',
  ];

  /**
   * Process Signifyd webhook events.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $signifyd_team
   *   The Signifyd team.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function events(Request $request, SignifydTeamInterface $signifyd_team);

}
