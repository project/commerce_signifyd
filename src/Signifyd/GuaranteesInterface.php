<?php

namespace Drupal\commerce_signifyd\Signifyd;

use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;

/**
 * Create or cancel Signifyd guarantee.
 */
interface GuaranteesInterface {

  /**
   * Submit a request to guarantee an existing case.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The response.
   */
  public function submit(SignifydCaseInterface $case): array;

  /**
   * Cancel a Guarantee.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The response.
   */
  public function cancel(SignifydCaseInterface $case): array;

}
