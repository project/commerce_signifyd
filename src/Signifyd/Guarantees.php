<?php

namespace Drupal\commerce_signifyd\Signifyd;

use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;
use Drupal\Core\Utility\Error;

/**
 * {@inheritdoc}
 */
class Guarantees extends SignifydAbstract implements GuaranteesInterface {

  /**
   * {@inheritdoc}
   */
  public function submit(SignifydCaseInterface $case): array {
    try {
      $response = $this->signifydClient->sendGuarantee($case);

      if ($this->logging) {
        $this->logger->notice(t('<b>Response:</b> <pre><code>@response</code></pre>', [
          '@response' => print_r($response, TRUE),
        ]));
      }
      return $response;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function cancel(SignifydCaseInterface $case): array {
    try {
      $response = $this->signifydClient->cancelGuarantee($case);

      if ($this->logging) {
        $this->logger->notice(t('<b>Response:</b> <pre><code>@response</code></pre>', [
          '@response' => print_r($response, TRUE),
        ]));
      }

      return $response;
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }
    return [];
  }

}
