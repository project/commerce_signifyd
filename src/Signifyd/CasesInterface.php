<?php

namespace Drupal\commerce_signifyd\Signifyd;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_signifyd\Entity\SignifydCaseInterface;

/**
 * Create or get Signifyd Case.
 */
interface CasesInterface {

  /**
   * Submit a case fraud review.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The case data.
   *
   * @return array
   *   The response.
   */
  public function createCase(OrderInterface $order): array;

  /**
   * Retrieve details about an individual case by investigation id or case id.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case
   *   The Signifyd case.
   *
   * @return array
   *   The response.
   */
  public function getCase(SignifydCaseInterface $case): array;

}
