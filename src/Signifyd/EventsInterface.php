<?php

namespace Drupal\commerce_signifyd\Signifyd;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Update Signifyd based off order fulfillment.
 */
interface EventsInterface {

  /**
   * Map commerce default shipment workflow to Signifyd.
   *
   * @see commerce_shipping.workflows.yml
   */
  public const COMMERCE_SIGNIFYD_MAP_SHIPMENT_STATUS = [
    'draft' => 'waiting for pickup',
    'ready' => 'waiting for pickup',
    'shipped' => 'delivered',
    'canceled' => 'exception',
  ];

  /**
   * Order Fulfilled.
   *
   * A fulfillment represents a shipment of one or more items in an order.
   * Submit fulfillment details for orders that you have shipped,
   * even if the order is partially shipped.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return array
   *   The response.
   */
  public function orderFulfilled(OrderInterface $order): array;

}
