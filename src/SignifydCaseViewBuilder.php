<?php

namespace Drupal\commerce_signifyd;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Render controller for signifyd case entities.
 */
class SignifydCaseViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $defaults = parent::getBuildDefaults($entity, $view_mode);
    $defaults['#theme'] = 'signifyd_case';
    return $defaults;
  }

}
