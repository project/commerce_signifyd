<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides interface for SignifydCaseStorage.
 */
interface SignifydCaseStorageInterface {

  /**
   * Load case by order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_signifyd\Entity\SignifydCase[]
   *   The signifyd case entity.
   */
  public function loadByOrder(OrderInterface $order);

}
