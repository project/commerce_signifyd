<?php

namespace Drupal\commerce_signifyd\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Signifyd team entity type.
 */
interface SignifydTeamInterface extends ConfigEntityInterface {

  /**
   * The Signifyd api key.
   *
   * @return string
   *   The api key.
   */
  public function getApiKey(): string;

  /**
   * Set the api key.
   *
   * @return $this
   *   The signifyd_team entity.
   */
  public function setApiKey(string $api_key): static;

  /**
   * The Signifyd team id.
   *
   * @return int
   *   The team id.
   */
  public function getTeamId(): int;

  /**
   * Set the team id.
   *
   * @return $this
   *   The signifyd_team entity.
   */
  public function setTeamId(int $team_id): static;

}
