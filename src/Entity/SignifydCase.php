<?php

namespace Drupal\commerce_signifyd\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the Case entity class.
 *
 * @ContentEntityType(
 *   id = "signifyd_case",
 *   label = @Translation("Case"),
 *   label_singular = @Translation("case"),
 *   label_plural = @Translation("cases"),
 *   label_count = @PluralTranslation(
 *     singular = "@count case",
 *     plural = "@count cases",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "list_builder" = "\Drupal\commerce_signifyd\SignifydCaseListBuilder",
 *      "view_builder" = "Drupal\commerce_signifyd\SignifydCaseViewBuilder",
 *     "storage" = "Drupal\commerce_signifyd\SignifydCaseStorage",
 *     "storage_schema" = "Drupal\commerce_signifyd\SignifydCaseStorageSchema",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "signifyd_case",
 *   internal = TRUE,
 *   entity_keys = {
 *     "id" = "case_id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class SignifydCase extends ContentEntityBase implements SignifydCaseInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOrderId() {
    return $this->get('order_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder() {
    return $this->get('order_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getCaseId();
  }

  /**
   * {@inheritdoc}
   */
  public function getInvestigationId() {
    return $this->get('investigation_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setInvestigationId($investigation_id) {
    $this->set('investigation_id', $investigation_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCaseId() {
    return $this->get('case_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getScore() {
    return $this->get('score')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setScore($score) {
    $this->set('score', (int) $score);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getGuarantee() {
    return $this->get('guarantee')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setGuarantee($guarantee) {
    $this->set('guarantee', $guarantee);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDecision() {
    return $this->get('decision')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDecision($decision) {
    $this->set('decision', $decision);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTeam() {
    return $this->get('team_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setTeam(SignifydTeamInterface $team) {
    $this->set('team_id', $team);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['case_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Case ID'))
      ->setSetting('size', 'big')
      ->setDescription(t('The Signifyd case ID'));

    $fields['order_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setDescription(t('The order for the case.'))
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default');

    $fields['team_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Signifyd team'))
      ->setDescription(t('The team for the case.'))
      ->setSetting('target_type', 'signifyd_team')
      ->setSetting('handler', 'default');

    $fields['score'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Score'))
      ->setDefaultValue(0)
      ->setDescription(t('The Signifyd score'));

    $fields['guarantee'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Guarantee'))
      ->setDescription(t('The Signifyd guarantee'));

    $fields['decision'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Decision'))
      ->setDescription(t('The Signifyd decision'));

    $fields['investigation_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Investigation ID'))
      ->setSetting('size', 'big')
      ->setDescription(t('The Signifyd investigation ID'));

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The Signifyd case status'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the log was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the log was created.'));

    return $fields;
  }

}
