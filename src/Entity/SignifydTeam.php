<?php

namespace Drupal\commerce_signifyd\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Signifyd team entity type.
 *
 * @ConfigEntityType(
 *   id = "signifyd_team",
 *   label = @Translation("Signifyd team"),
 *   label_collection = @Translation("Signifyd teams"),
 *   label_singular = @Translation("Signifyd team"),
 *   label_plural = @Translation("Signifyd teams"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Signifyd team",
 *     plural = "@count Signifyd teams",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\commerce_signifyd\SignifydTeamListBuilder",
 *     "form" = {
 *       "default" = "Drupal\commerce_signifyd\Form\SignifydTeamForm",
 *       "add" = "Drupal\commerce_signifyd\Form\SignifydTeamForm",
 *       "edit" = "Drupal\commerce_signifyd\Form\SignifydTeamForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "signifyd_team",
 *   admin_permission = "administer signifyd_team",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "team_id",
 *     "api_key"
 *   },
 *   links = {
 *     "collection" = "/admin/commerce/config/signifyd/team",
 *     "add-form" = "/admin/commerce/config/signifyd/team/add",
 *     "edit-form" = "/admin/commerce/config/signifyd/team/{signifyd_team}",
 *     "delete-form" = "/admin/commerce/config/signifyd/team/{signifyd_team}/delete"
 *   }
 * )
 */
class SignifydTeam extends ConfigEntityBase implements SignifydTeamInterface {

  /**
   * The Signifyd team entity ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The Signifyd team label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The Signifyd team api key.
   *
   * @var string
   */
  protected string $api_key = '';

  /**
   * The Signifyd team id.
   *
   * @var int
   */
  protected int $team_id = 0;

  /**
   * {@inheritdoc}
   */
  public function getApiKey(): string {
    return $this->api_key;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey(string $api_key): static {
    $this->api_key = $api_key;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTeamId(): int {
    return $this->team_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setTeamId(int $team_id): static {
    $this->team_id = $team_id;
    return $this;
  }

}
