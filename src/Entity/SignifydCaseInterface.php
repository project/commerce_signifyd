<?php

namespace Drupal\commerce_signifyd\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides interface for SignifidCase entity.
 */
interface SignifydCaseInterface extends ContentEntityInterface {

  public const SIGNIFYD_DECISION_ACTION = [
    'ACCEPT',
    'HOLD',
    'REJECT',
  ];

  public const SIGNIFYD_GUARANTEE_DISPOSITION = [
    'APPROVED',
    'DECLINED',
    'PENDING',
    'CANCELED',
    'IN_REVIEW',
  ];

  /**
   * Gets the case ID.
   *
   * @return int|null
   *   The Signifyd Case ID.
   */
  public function getCaseId();

  /**
   * Gets the order ID.
   *
   * @return int|null
   *   The order ID.
   */
  public function getOrderId();

  /**
   * Gets the order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order entity.
   */
  public function getOrder();

  /**
   * Get the Signifyd score.
   *
   * @return int
   *   The Signifyd investigation id.
   */
  public function getInvestigationId();

  /**
   * Set the Signifyd score.
   *
   * @param int $investigation_id
   *   The Signifyd investigation id.
   *
   * @return int
   *   The Signifyd investigation id.
   */
  public function setInvestigationId($investigation_id);

  /**
   * Gets the Signifyd score.
   *
   * @return int
   *   The Signifyd score.
   */
  public function getScore();

  /**
   * Set the Signifyd score.
   *
   * @param int $score
   *   The Signifyd score.
   *
   * @return int
   *   The Signifyd score.
   */
  public function setScore(int $score);

  /**
   * Gets the Signifyd guarantee.
   *
   * @return string
   *   The Signifyd guarantee.
   */
  public function getGuarantee();

  /**
   * Set the Signifyd guarantee.
   *
   * @param string $guarantee
   *   The Signifyd guarantee.
   *
   * @return int
   *   The Signifyd guarantee.
   */
  public function setGuarantee($guarantee);

  /**
   * Gets the Signifyd decision.
   *
   * @return string
   *   The Signifyd decision.
   */
  public function getDecision();

  /**
   * Set the Signifyd decision.
   *
   * @param string $decision
   *   The Signifyd decision.
   *
   * @return int
   *   The Signifyd decision.
   */
  public function setDecision($decision);

  /**
   * Gets the Signifyd status.
   *
   * @return string
   *   The Signifyd status.
   */
  public function getStatus();

  /**
   * Set the Signifyd status.
   *
   * @param string $status
   *   The Signifyd status.
   *
   * @return int
   *   The Signifyd status.
   */
  public function setStatus($status);

  /**
   * Gets the case creation timestamp.
   *
   * @return int
   *   Creation timestamp of the case.
   */
  public function getCreatedTime();

  /**
   * Sets the case creation timestamp.
   *
   * @param int $timestamp
   *   The case creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the Signifyd team.
   *
   * @return \Drupal\commerce_signifyd\Entity\SignifydTeamInterface
   *   The Signifyd team.
   */
  public function getTeam();

  /**
   * Set the Signifyd team.
   *
   * @param \Drupal\commerce_signifyd\Entity\SignifydTeamInterface $team
   *   The Signifyd team.
   *
   * @return $this
   *   The Signifyd.
   */
  public function setTeam(SignifydTeamInterface $team);

}
