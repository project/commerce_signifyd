<?php

namespace Drupal\commerce_signifyd;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Helper for processing tasks related to Signifyd integration.
 */
interface SignifydTaskInterface {

  const OPERATION_CREATE_CASE = 'create_case';

  const OPERATION_CANCEL_GUARANTEE = 'cancel_guarantee';

  const OPERATION_SEND_FULFILLMENT = 'send_fulfillment';

  /**
   * Send data to Signifyd based on operation.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $operation
   *   The operation.
   */
  public function processOrderOperation(OrderInterface $order, string $operation);

  /**
   * Processe order state transition.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param string $operation
   *   The operation.
   */
  public function processOrderEvent(OrderInterface $order, string $operation);

}
