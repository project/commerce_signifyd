<?php

namespace Drupal\commerce_signifyd;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Defines the log schema handler.
 */
class SignifydCaseStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);
    $schema[$this->storage->getBaseTable()]['indexes'] += [
      'order_id' => [
        'order_id',
      ],
      'guarantee' => [
        'guarantee',
      ],
    ];
    return $schema;
  }

}
