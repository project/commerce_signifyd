CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Webhook & Decision type
* Commerce 2 integration
* Custom integration
* Snippets

INTRODUCTION
------------
This module integrates [Signifyd](https://signifyd.grsm.io/4ub1gqjh9mb7) with Commerce Core
to add fraud prevention services to Drupal Commerce sites.

**1.x version of Commerce Signifyd module integrates with
[Signifyd v2 API](https://developer.signifyd.com/api-v2/)**

The module provides options to customize where and how the integration
is being used.

**Features**
* use Drupal queue to process all requests towards Signifyd.
* enable Signifyd integration per order type
* enable automatic transitions based on Signifyd output.
* choice on which Signifyd parameter automatic transitions should occur:
Guarantee, Decision or Score.
* choice to exclude on specific criteria (payment gateway and/or customer role)
orders which should not be processed and sent to Signifyd.
* use single or multiple Signifyd teams

**Available submodules provided with current integrations are:**
* device_fingerprint - tracks and identifies devices used to transact on your
site, increasing your protection from fraud.
* user_order_data - provides aggregate orders data per user. Require
additional dependency because of need to convert all amounts to USD:
[Commerce Exchanger](https://www.drupal.org/project/commerce_exchanger)

REQUIREMENTS
------------
This module requires Drupal Commerce 2 and it's submodules order, payment
and log.

INSTALLATION
------------
Install the Commerce Signifyd module as you would normally install
any Drupal contrib module.
Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
--------------
1. Navigate to Administration > Extend > and enable the Commerce Signifyd
module.
2. Navigate to Home > Administration > Commerce > Configuration >
      Signifyd > Teams.
3. Create a team with Team ID and API key - _obtain from a team on
Signifyd dashboard_
4. Navigate to Home > Administration > Commerce > Configuration >
Signifyd > Settings.
5. Select default Signifyd team.
5. Choose decision type which you want to use _(see section below)_.
6. Enable webhooks - _on Signifyd website under team
settings page go to Notifications, and set up webhooks to point towards
https://yourwebsite.com/webhook/signifyd/{team_id}_ _(see section below)_.
7. Enable integration for one or multiple order types.
8. Enable automatic workflow integration and choose appropriate
order transitions which going to be triggered based on Signifyd
decision type.
9. Choose conditions for exclusion if applicable.

If you need to disable integration on some environments (e.g. local, develop, stage etc.)
you can do it per enabled order types through config override.

If you have an order type with the machine name `default` for which integration is enabled.
To disable it on a specific environment, you can use config override:
$config['commerce_signifyd.settings']['order_types']['default']['enable'] = 0;

The second option would be to use the [config_split](https://www.drupal.org/project/config_split) module.

DECISION TYPE & WEBHOOK SETUP
--------------
| Webhook (Signifyd)       |  Decision type (Drupal)   |
|:-------------------------|:-------------------------:|
| Decision Made            |         DECISION          |
| Case Creation            |         GUARANTEE         |
| Case Rescore             |         GUARANTEE         |
| Case Review              |         GUARANTEE         |

_Decision type SCORE from Drupal is available as option to be used in
advanced scenarios.
In Drupal integration it can use any of the listed webhooks.
Do note that using only score does not provide you necessary
with financial guarantee or decision._

COMMERCE INTEGRATION
--------------
Integration is built around order transitions:
* place - when order is placed, the payload for Signifyd case is sent.
* fulfill - when order is fulfilled, the payload for shipments is sent.
* cancel - when order is canceled, call to Signifyd is made to cancel
existing Guarantee

Each Signifyd case entry sent from Drupal is represented by entity
called `Signifyd Case`. This entity is created once Signifyd send updates
trough configured webhooks.

**List of Signifyd Cases**
* Navigate to: Home > Administration > Commerce > Signifyd Cases to
  see all cases created

CUSTOM INTEGRATIONS
--------------
If you have need for more customizable solution and manipulation
withing your workflow, and don't want provided out of box transitioning,
you can write your module and use event `SignifydWebhookEvent`.

This event gives you ability to react on webhook updates provided
from Signifyd where you have available the Signifyd Case, Commerce Order.

Additionally two more events are triggered during:
* order placement - you can alter payload prior sending to Signifyd
* order fulfillment - you can alter payload prior sending to Signifyd

SNIPPETS
--------------

Get all Signifyd cases for an order.
```
$order = Order::load(1);
$case_storage = \Drupal::entityTypeManager()->getStorage('signifyd_case');
// Get all cases for order - array of cases.
$cases = $case_storage->loadByOrder($order);
```

Event subscriber on webhook update (SignifydWebhookEvent).
In this example we are moving order based on Signifyd Guarantee
with specific transition.

```
services:
  your_module.signifyd_webhook:
    class: Drupal\your_module\EventSubscriber\SignifydWebhookSubscriber
    arguments: []
    tags:
      - { name: event_subscriber }
```

```
<?php

namespace Drupal\your_module\EventSubscriber;

use Drupal\commerce_signifyd\Event\SignifydEvents;
use Drupal\commerce_signifyd\Event\SignifydWebhookEvent;
use Drupal\commerce_signifyd\Signifyd\WebhookInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Example of webhook event subscriber.
 */
class SignifydWebhookSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SignifydEvents::SIGNIFYD_WEBHOOK => ['onWebhook'],
    ];
  }

  /**
   * @param \Drupal\commerce_signifyd\Event\SignifydWebhookEvent $event
   *   The event.
   */
  public function onWebhook(SignifydWebhookEvent $event) {
    if (!in_array($event->getTopic(), [
      WebhookInterface::SIGNIFYD_CASE_CREATION,
      WebhookInterface::SIGNIFYD_CASE_REVIEW,
      WebhookInterface::SIGNIFYD_DESCISION_MADE,
    ])) {
      return;
    }

    $signifyd_case = $event->getSignifydCase();
    $order = $signifyd_case->getOrder();

    if (!$order) {
      return;
    }

    // Only react on orders in specific state.
    if ($order->getState()->getId() !== 'ready_for_fraud_check') {
      return;
    }

    if ($signifyd_case->getGuarantee() === "APPROVED") {
      $order->getState()->applyTransitionById('fraud_check_passed');
    }
    else {
      $order->getState()->applyTransitionById('fraud_check_flagged');
    }

    $order->save();
  }

}

```
