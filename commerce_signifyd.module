<?php

/**
 * @file
 * Hook implementations for the commerce_signifyd module.
 */

use Drupal\commerce_signifyd\SignifydClientInterface;
use Drupal\Core\Render\Element;

/**
 * Implements hook_theme().
 */
function commerce_signifyd_theme() {
  return [
    'signifyd_case' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for signifyd case templates.
 *
 * Default template: signifyd-case.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing rendered fields.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_signifyd_case(array &$variables) {
  /** @var \Drupal\commerce_signifyd\Entity\SignifydCaseInterface $case */
  $case = $variables['elements']['#signifyd_case'];

  $variables['view_mode'] = $variables['elements']['#view_mode'];
  $variables['signifyd_case'] = $case;
  $variables['case_id'] = $case->id();
  $variables['case_url'] = SignifydClientInterface::SIGNIFYD_WEB_URL . 'orders/' . $case->id();
  $variables['case_score'] = $case->getScore();
  $variables['case_guarantee'] = $case->getGuarantee();
  $variables['case_status'] = $case->status->value;
  $variables['case_decision'] = $case->getDecision();
  // Helpful $content variable for templates.
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_preprocess_commerce_order().
 */
function commerce_signifyd_preprocess_commerce_order(&$variables) {
  $entity_type_manager = \Drupal::entityTypeManager();
  /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
  $order = $variables['elements']['#commerce_order'];
  $signifyd_config = \Drupal::config('commerce_signifyd.settings');
  $signifyd_enabled = $signifyd_config->get('order_types.' . $order->bundle() . '.enable');
  if ($signifyd_enabled) {
    /** @var \Drupal\commerce_signifyd\SignifydCaseStorageInterface $signifyd_storage */
    $signifyd_storage = $entity_type_manager->getStorage('signifyd_case');
    $signifyd_view_builder = $entity_type_manager->getViewBuilder('signifyd_case');
    if ($signifyd_cases = $signifyd_storage->loadByOrder($order)) {
      $variables['additional_order_fields']['signifyd_case'] = $signifyd_view_builder->viewMultiple($signifyd_cases);
    }
  }
}
